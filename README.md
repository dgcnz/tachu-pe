# Tachu Web

Simple non-profit test landing page.

## Content

### ¿Qué es tachu.pe?

Tachu es una startup que busca incentivar el reciclaje a través de tachos recicladores.

* **Únete**: Descarga la app y cera un perfil para mantener tus créditos
* **Recicla**: Registra tu basura en nuestros tachus a través de la app y acumula puntos.
* **Canjea**: Gana premios conforme vayas ganando puntos.
* **Logística**: No te preocupes, la recolección de los tachus y el subsiguiente reciclaje es trabajo nuestro.

### Nuestros logros

Cuál ha sido el impacto de tachu.pe.

* 1452 tree cut
* 1458 animal lost
* 9854 blubs collected
* 5412 water level

### ¿Dónde encontrarnos?

En estros lugares podrás encontrar tachus para reciclar.

* Real Plaza Salaverry
    * Av. Gral. Salaverry 2370, Jesús María
    * Lorem ipsum dolor sit amet, consectetur adip scing elit. Lorem ipsum dolor sit amet,consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
* Feria Marciana
    * San Borja 15036
    * Lorem ipsum dolor sit amet, consectetur adip scing elit. Lorem ipsum dolor sit amet,consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
* Lorem ipsum
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.
* Lorem ipsum
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.
* Lorem ipsum
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.
* Lorem ipsum
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.
* Lorem ipsum
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.

### Testimonios

* Sadequr Rahman Sojib. CEO, Fourder
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.
* Sadequr Rahman Sojib. CEO, Fourder
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.
* Sadequr Rahman Sojib. CEO, Fourder
    * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.

### Sponsors

* Real Plaza
* Lorem Ipsum
* Lorem Ipsum
* Lorem Ipsum

### Contáctnos

¿Eres una feria, empresa, o centro compercial? Anímate a contactarnos.

* Nuestros HQ:
    * Jr. Medrano Silva 165, Barranco 15063
    * (01) 0000000
    * contacto@tachu.com

* Déjanos un mensaje
    * Nombre
    * Correo
    * Asunto
    * Mensaje
